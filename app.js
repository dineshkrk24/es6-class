// let var and const

function randomLoop() {
  for (var i = 0; i < 5; i++) {
    console.log(i);
  }
  console.log(i);
}

randomLoop();

const age = 20;
// age = 21;

// array methods in js
// const college = [
//   { id: 1, name: "CITE", address: { city: "ktm", street: "Tinkune" } },
//   {
//     id: 2,
//     name: "Nobile college",
//     address: { city: "ktm", street: "Tinkune" },
//   },
//   {
//     id: 3,
//     name: "Everest College",
//     address: { city: "ktm", street: "Tinkune" },
//   },
//   { id: 4, name: "Nami College", address: { city: "ktm", street: "Tinkune" } },
// ];

// console.log(college);

// foreach loop

// college.forEach(function (value, index, allCollegeData) {
//   console.log(value);
//   console.log(index);
//   console.log(allCollegeData);
// });

// const newCollegeData = college.map(function (value, index, allCollegeData) {
//   console.log(value);
//   console.log(index);
//   console.log(allCollegeData);
//   return value.id === 1;
// });

// const newCollegeData = college
//   .filter(function (value, index, allCollegeData) {
//     console.log(value);
//     // console.log(index);
//     // console.log(allCollegeData);
//     // return value.id !== 1;
//   })
//   .map(function (data) {
//     console.log(data);
//   });

// console.log(newCollegeData);

const numbers = [1, 2, [333], 4, 5];

// const totalPrice = numbers.reduce(function (acc, curVal) {
//   console.log(acc);
//   console.log(curVal);
//   return acc + curVal;
// });

// console.log(totalPrice);

const [a, b, [c]] = numbers;
console.log(a, b);
console.log(c);

// const toogle =

const college = {
  id: 1,
  name: "CITE",
  address: { city: "ktm", street: "Tinkune" },
};

const {
  id,
  name,
  address: { street, city },
} = college;
console.log(id);
console.log(name);
console.log(street);
console.log(city);

// spread operator

let num1 = [1, 2, 3, 4];
let num2 = [...num1, 5, 6, 7];

console.log(num2);

// rest operator

const [aa, bb, ...rest] = num1;
console.log(rest);

// addNum(...rest)

// optional chaining

console.log(college.address.add?.name);

// Nullish coalish operator
let per = 0;

// const perNumber = per || 10;
const perNumber = per ?? 10; //(null and undefined)

console.log(perNumber);

for (let item of num1) {
  console.log(item);
}
